# GobiernoEspiaJSON
Archivo JSON de los SMS documentados por [Article19](https://articulo19.org), [R3D - Red en Defensa de los Derechos Digitales](https://r3d.mx) y [SocialTIC](https://socialtic.org/).

Consulta y descarga aquí: [GobiernoEspia.json](https://raw.githubusercontent.com/Escenaconsejo/GobiernoEspiaJSON/master/GobiernoEspia.json)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

Referencias:
* [Gobierno Espía: Vigilancia sistemática a periodistas y defensores de derechos humanos en México](https://r3d.mx/gobiernoespia) (Junio 2017) - [Article19](https://articulo19.org), [R3D - Red en Defensa de los Derechos Digitales](https://r3d.mx) y [SocialTIC](https://socialtic.org/)
* [Confirman que el GIEI también fue objetivo del #GobiernoEspía](https://r3d.mx/2017/07/10/giei-gobierno-espia/) (10 de julio, 2017) R3D
* [#GobiernoEspía: 40 días después](https://r3d.mx/2017/07/28/gobiernoespia-40-dias-despues/) (28 de julio, 2017) R3D
* [#GobiernoEspía: Representantes de víctimas del caso Narvarte fueron objetivo de Pegasus](https://r3d.mx/2017/08/02/gobierno-espia-caso-narvarte/) (2 de agosto, 2017) R3D
